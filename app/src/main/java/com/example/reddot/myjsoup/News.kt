package com.example.reddot.myjsoup

class News(
    val id: String,
    val title: String,
    val description: String,
    val linkImage: String,
    val additionalInfo: String
)