package com.example.reddot.myjsoup

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jsoup.Jsoup

class MainActivity : AppCompatActivity() {

    private val topUrl = "http://rutor.info/top"
    private val url = "https://www.volzsky.ru/index.php?wx=16"
    private val listNews = mutableListOf<News>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GlobalScope.launch {
            getData()
        }

    }

    private fun getData() {

        val document = Jsoup.connect(topUrl).get()
        val element = document
            .select("div[id=index]")
            .select("h2")
//            .select("a")

        for (i in 0 until element.size) {
            val title = element
                .eq(i)
                .text()
            listNews.add(News("id", title, "description", "linkImage", "additionalInfo"))
        }

        listNews.size

    }
}
